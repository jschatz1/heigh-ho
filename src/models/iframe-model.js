export default class IframeModel {
  constructor() {
    this.baseJS = `
    postMessage = (message) ->
      window.top.postMessage(message, '*');

    rand = (length, current) ->
      current = if current then current else ''
      if length then rand(--length, '0123456789abcdefghiklmnopqrstuvwxyz'.charAt(Math.floor(Math.random() * 60)) + current) else current

    insert = (options={}) ->
      parent = options.parent || 'body'
      tag = options.tag || 'div'
      id = options.id || "insert-#{rand(10)}"
      content = options.content || ''
      $(parent).append("<#{tag} id=\\"#{id}\\">#{content}</#{tag}>")

    i = insert

    css = (key, val,identifier='body') ->
      $(identifier).css(key, val)

    clear = ->
      $('body').html('')

    `.trim();
    this.userHTML = '';
    this.baseUserJS = '';
    this.baseInclude = `
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous"></script>
    `.trim();
    this.baseHTML = `
    <html>
    <head></head>
    <body>{{body}}</body>
    </html>
    `.trim();
  }
}
