import Model from '../models/iframe-model';
import CoffeeScript from './coffee-script';

export default class Parse {
  constructor() {
    this.model = new Model();
  }

  htmlFromCommand(command) {
    let scripts = this.buildScripts(command);
    if (scripts.success) {
      scripts = `<script>${scripts.code}</script>`;
    } else {
      return { success: false, error: scripts.error };
    }
    let html = `
    ${this.model.userHTML}
    ${this.model.baseInclude}
    ${scripts}
    `.trim();
    html = this.model.baseHTML.replace('{{body}}', html);
    return { success: true, html };
  }

  buildScripts(command) {
    const oldJS = this.model.baseUserJS;
    this.model.baseUserJS = `${this.model.baseUserJS}\ntry ${command} catch e then postMessage(e.toString())`;
    const fullScript = `${this.model.baseJS}\n${this.model.baseUserJS}`;
    const result = {};
    try {
      result.success = true;
      result.code = CoffeeScript().compile(fullScript);
    } catch (e) {
      result.success = false;
      result.error = e;
      this.model.baseUserJS = oldJS;
    }
    return result;
  }

  static insertDoc(html) {
    const iFrame = document.getElementById('doc').contentWindow.document;
    iFrame.open('text/html', 'replace');
    iFrame.write(html);
    iFrame.close();
  }
}
