import $ from 'jquery';
import Vue from 'vue';
import App from './App';

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  render: h => h(App),
});

window.onmessage = function onm(e) {
  if (typeof e.data === 'string') {
    $(document).trigger('alertmessage', e.data);
  }
};
